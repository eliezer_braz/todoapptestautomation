package automationFramework;

import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class UserStoryTwoTest {
	
	private static WebDriver driver;
	private static WebElement modal;
	
	@BeforeClass
	public static void init() {
		driver = new FirefoxDriver(); 
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://qa-test.avenuecode.com/");
        HomePage.signIn(driver).click(); 
        LoginPage.emailTextField(driver).sendKeys("eliezer.braz@gmail.com");
        LoginPage.passwordTextField(driver).sendKeys("gabilinda01");
        LoginPage.commitButton(driver).click();
        MyTasksInitPage.myTasksButton(driver).click();
        if (MyTasksPage.tasksList(driver).isEmpty()) {
        	MyTasksPage.newTaskForm(driver).sendKeys("new task for test");
        	MyTasksPage.newTaskForm(driver).sendKeys(Keys.ENTER);
        }
        MyTasksPage.tasksList(driver).get(0).click();
        modal = driver.switchTo().activeElement();
	}
	
	@Test 
	public void testCheckTestId() {
		String taskId = SubTaskWindow.taskId(modal).getText();
		String[] values = taskId.split(" ");
		assertTrue("This field should have three items.", values.length == 3);
	}
	
	@Test 
	public void testReadOnlyField() {
		SubTaskWindow.taskDescription(modal).sendKeys("Test value");
		String value = SubTaskWindow.taskDescription(modal).getAttribute("value");
		assertFalse("This field should be a read-only field", value.indexOf("Test value") != -1);
	}
	
	@Test
	public void testNewSubTaskByEnter() {		
        int initSize = SubTaskWindow.subTaskList(modal).size();
        SubTaskWindow.subTaskForm(modal).sendKeys("testNewSubTaskByEnter 01");
        SubTaskWindow.dueDateForm(modal).sendKeys("12/12/2017");
        SubTaskWindow.subTaskForm(modal).sendKeys(Keys.ENTER);
        int endSize = SubTaskWindow.subTaskList(modal).size();
        assertEquals("Subtasks should only be added using add button", initSize, endSize);
	}
	
	
	@Test
	public void testNewSubTaskByAddKey() {		
        int initSize = SubTaskWindow.subTaskList(modal).size();
        SubTaskWindow.subTaskForm(modal).sendKeys("testNewSubTaskByEnter 01");
        SubTaskWindow.dueDateForm(modal).sendKeys("12/12/2017");
        SubTaskWindow.addButton(modal).click();
        int endSize = SubTaskWindow.subTaskList(modal).size();
        assertTrue("Subtasks should have been added.", initSize < endSize);
	}
	
	@Test 
	public void testMoreThan250CharacterSubTask() {
		int limit = 250;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 251; i++) {
			sb.append('a');
		}
		String input = sb.toString();
        SubTaskWindow.subTaskForm(modal).sendKeys(input);
        int curSize = SubTaskWindow.subTaskForm(modal).getAttribute("value").length();        
        assertTrue("Subtask should be limited to 250 characters", curSize <= limit);
	}
	
	@Test
	public void testDueDateLetters() {
		String date = "mm/dd/yyyy";
		int initSize = SubTaskWindow.subTaskList(modal).size();
		SubTaskWindow.subTaskForm(modal).sendKeys("testDueDateLetters");
		SubTaskWindow.dueDateForm(modal).clear();
		SubTaskWindow.dueDateForm(modal).sendKeys(date);
		SubTaskWindow.addButton(modal).click();
        int endSize = SubTaskWindow.subTaskList(modal).size(); 
		assertEquals("A subtask with invalid values for due date field should not be added", initSize,endSize);
	}
	
	@Test
	public void testDueDateWrongValues() {
		String date = "70/70/1111111";
		int initSize = SubTaskWindow.subTaskList(modal).size();
		SubTaskWindow.subTaskForm(modal).sendKeys("testDueDateWrongValues");
		SubTaskWindow.dueDateForm(modal).clear();
		SubTaskWindow.dueDateForm(modal).sendKeys(date);
		SubTaskWindow.addButton(modal).click();
        int endSize = SubTaskWindow.subTaskList(modal).size(); 
		assertEquals("A subtask with invalid values for due date field should not be added", initSize,endSize);
	}
	
	@Test 
	public void testNewSubTaskPosition() {
		String name = "testNewSubTaskPosition";
		SubTaskWindow.subTaskForm(modal).sendKeys("dummy task");
		SubTaskWindow.addButton(modal).click();		
		SubTaskWindow.subTaskForm(modal).sendKeys(name);
		SubTaskWindow.dueDateForm(modal).clear();
		SubTaskWindow.dueDateForm(modal).sendKeys("12/12/2016");
		SubTaskWindow.addButton(modal).click();
		List<WebElement> list = SubTaskWindow.subTaskList(modal);
		String last = list.get(list.size()-1).getText();
		assertTrue("New subtask should be the last element", last.indexOf(name) != -1 );
		
	}
	
	@Test
	public void testEmptySubTaskDescription() {		
        int initSize = SubTaskWindow.subTaskList(modal).size();
        SubTaskWindow.subTaskForm(modal).clear();
        SubTaskWindow.dueDateForm(modal).sendKeys("12/12/2017");
        SubTaskWindow.addButton(modal).click();
        int endSize = SubTaskWindow.subTaskList(modal).size();
        assertEquals("Subtasks should not be added since SubTask Description field is empty", initSize, endSize);
	}
	
	@Test
	public void testEmptyDueDate() {		
        int initSize = SubTaskWindow.subTaskList(modal).size();
        SubTaskWindow.subTaskForm(modal).sendKeys("testEmptyDueDate 01");
        SubTaskWindow.dueDateForm(modal).clear();
        SubTaskWindow.addButton(modal).click();
        int endSize = SubTaskWindow.subTaskList(modal).size();
        assertEquals("Subtasks should not be added since Due Date field is empty", initSize, endSize);
	}
	
	
	
	@AfterClass
	public static void finish() {
		driver.close();
	}

}
