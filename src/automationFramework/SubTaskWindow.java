package automationFramework;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SubTaskWindow {
	
	public static WebElement taskId(WebElement modal)  {
		return modal.findElement(By.className("modal-title"));
	}
	
	public static WebElement taskDescription(WebElement modal)  {
		return modal.findElement(By.id("edit_task"));
	}
	
	public static WebElement subTaskForm(WebElement modal) {
		return modal.findElement(By.id("new_sub_task"));		
	}
	
	public static WebElement dueDateForm(WebElement modal) {
		return modal.findElement(By.id("dueDate"));		
	}
	
	public static WebElement addButton(WebElement modal) {
		return modal.findElement(By.id("add-subtask"));		
	}
	
	public static WebElement closeButton(WebElement modal) {
		return modal.findElement(By.xpath("//button[@ng-click='close()']"));
	}
	
	public static List<WebElement> subTaskList(WebElement modal) {
		return  modal.findElements(By.tagName("tr"));
	}
	
	public static List<WebElement> checkBoxList(WebElement modal) {
		return modal.findElements(By.xpath("//input[@type='checkbox']"));
	}
	
	public static List<WebElement> editBoxList(WebElement modal) {
		return modal.findElements(By.xpath("//a[@onaftersave='updateSubTask(sub_task)']"));
	}
	

}
