package automationFramework;

import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RandomTests {
	
	private static WebDriver driver;
	private static WebElement modal;
	
	@BeforeClass	
	public static void init() {
		driver = new FirefoxDriver(); 
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://qa-test.avenuecode.com/");
        HomePage.signIn(driver).click(); 
        LoginPage.emailTextField(driver).sendKeys("eliezer.braz@gmail.com");
        LoginPage.passwordTextField(driver).sendKeys("gabilinda01");
        LoginPage.commitButton(driver).click();
        MyTasksInitPage.myTasksButton(driver).click();
        if (MyTasksPage.tasksList(driver).isEmpty()) {
        	MyTasksPage.newTaskForm(driver).sendKeys("new task for test testSubTasksSameName");
        	MyTasksPage.newTaskForm(driver).sendKeys(Keys.ENTER);        	
        }
        MyTasksPage.comboBoxList(driver).get(0).sendKeys(Keys.ENTER);
        MyTasksPage.tasksList(driver).get(0).click();
        modal = driver.switchTo().activeElement();
	}
	
	@Test
	public void testSubTasksSameName() {		
        String name = "testSubTasksSameName 01";
        SubTaskWindow.subTaskForm(modal).sendKeys(name);
        SubTaskWindow.dueDateForm(modal).sendKeys("12/12/2017");
        SubTaskWindow.addButton(modal).click();
        SubTaskWindow.subTaskForm(modal).sendKeys(name);
        SubTaskWindow.dueDateForm(modal).sendKeys("12/12/2017");
        SubTaskWindow.addButton(modal).click();
        List<WebElement> list = SubTaskWindow.editBoxList(modal);
                
        int count = 0;
        for (WebElement element : list) {
           	if (element.getText().indexOf(name) != -1) {
        		count++;
        	}
        }
        assertEquals("There should be only one element named testSubTasksSameName", 1, count);
	}
	
	@Test
	public void testUnmarkedWhenClosed() {
        SubTaskWindow.subTaskForm(modal).sendKeys("test subtask 01");
        SubTaskWindow.dueDateForm(modal).sendKeys("12/12/2016");
        SubTaskWindow.addButton(modal).click();
        SubTaskWindow.subTaskForm(modal).sendKeys("test subtask 02");
        SubTaskWindow.dueDateForm(modal).sendKeys("12/12/2016");
        SubTaskWindow.addButton(modal).click();
        SubTaskWindow.subTaskForm(modal).sendKeys("test subtask 03");
        SubTaskWindow.dueDateForm(modal).sendKeys("12/12/2016");
        SubTaskWindow.addButton(modal).click();
        List<WebElement> checkBoxes = SubTaskWindow.checkBoxList(modal);
        for (int i = 0; i < 2; i++) {
        	checkBoxes.get(i).sendKeys(Keys.ENTER);
        }
        SubTaskWindow.closeButton(modal).click();
        MyTasksPage.tasksList(driver).get(0).click();
        modal = driver.switchTo().activeElement();
        checkBoxes = SubTaskWindow.checkBoxList(modal);
        checkBoxes.get(checkBoxes.size()-1).sendKeys(Keys.ENTER);
        SubTaskWindow.closeButton(modal).click();
        MyTasksPage.tasksList(driver).get(0).click();
        modal = driver.switchTo().activeElement();
        checkBoxes = SubTaskWindow.checkBoxList(modal);
        boolean checked = true;
        for (int i = 0; i < 2; i++) {
        	if (!checkBoxes.get(i).isSelected()) {
        		checked = false;
        	}
        }
        assertTrue("Check boxes should remain selected", checked);
	}
	

	
	@AfterClass	
	public static void finish() {
		driver.close();
		
	}

}
