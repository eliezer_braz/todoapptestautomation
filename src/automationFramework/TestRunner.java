package automationFramework;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({
	UserStoryOneTest.class,
	UserStoryTwoTest.class,
	RandomTests.class })

public class TestRunner {
}
