package automationFramework;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class MyTasksPage {
	
	public static WebElement myTaskText(WebDriver driver) {
		return driver.findElement(By.xpath("//a[@href='/tasks']"));
	}
	
	public static WebElement table(WebDriver driver) {
		return driver.findElement(By.className("table"));		
	}
	
	public static WebElement newTaskForm(WebDriver driver) {
		return driver.findElement(By.id("new_task"));		
	}
	
	public static WebElement addButton(WebDriver driver) {
		return driver.findElement(By.xpath("//span[@ng-click='addTask()']"));		
	}
	
	public static List<WebElement> tasksList(WebDriver driver) {
		return driver.findElements(By.xpath("//button[@ng-click='editModal(task)']"));
	}
	
	public static List<WebElement> comboBoxList(WebDriver driver) {
		return driver.findElements(By.xpath("//input[@ng-model='task.completed']"));
	}
	

}
