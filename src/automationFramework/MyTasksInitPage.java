package automationFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MyTasksInitPage {

	private static WebElement element = null;
	
	public static WebElement myTasksButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@href='/tasks']"));
		return element;
	}
}
