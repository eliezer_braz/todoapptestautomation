package automationFramework;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;



public class UserStoryOneTest {
	
	private static WebDriver driver;

	@BeforeClass
	public static void openBrowser() {
		driver = new FirefoxDriver(); 
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://qa-test.avenuecode.com/");
        HomePage.signIn(driver).click(); 
        LoginPage.emailTextField(driver).sendKeys("eliezer.braz@gmail.com");
        LoginPage.passwordTextField(driver).sendKeys("gabilinda01");
        LoginPage.commitButton(driver).click();
        MyTasksInitPage.myTasksButton(driver).click();
	}
	
	@Test 
	public void testAddTaskEnter() {
		int initSize = MyTasksPage.tasksList(driver).size();
		MyTasksPage.newTaskForm(driver).sendKeys("testAddTask 01");
		MyTasksPage.newTaskForm(driver).sendKeys(Keys.ENTER);
		int afterSize = MyTasksPage.tasksList(driver).size();
		assertTrue("Task should should have been added", initSize < afterSize);
	}
	
	@Test 
	public void testAddTaskAddKey() {
		int initSize = MyTasksPage.tasksList(driver).size();
		MyTasksPage.newTaskForm(driver).sendKeys("testAddTaskAddKey 01");
		MyTasksPage.addButton(driver).click();;
		int afterSize = MyTasksPage.tasksList(driver).size();
		assertTrue("Task should should have been added", initSize < afterSize);
	}
	
	@Test 
	public void testMyTaskNavBar() {
		WebElement label = MyTasksPage.myTaskText(driver);
		String myTask = label.getText();
		String href = label.getAttribute("href");
		assertTrue("The user should always see the ‘My Tasks’ link on the NavBar", myTask.equals("My Tasks"));
		assertTrue("My Tasks should be a link", href != null && ! href.isEmpty() );
	}
	
	@Test 
	public void testLessThanThreeCharacterEnter() {		
		int initSize = MyTasksPage.tasksList(driver).size();
		MyTasksPage.newTaskForm(driver).sendKeys("ab");
		MyTasksPage.newTaskForm(driver).sendKeys(Keys.ENTER);
		int afterSize = MyTasksPage.tasksList(driver).size();
		assertEquals("Task should require at least 3 characters before enter, then task should have not been added", initSize,afterSize);
	}
	
	@Test 
	public void testLessThanThreeCharacterAddButton() {		
		int initSize = MyTasksPage.tasksList(driver).size();
		MyTasksPage.newTaskForm(driver).sendKeys("cd");
		MyTasksPage.addButton(driver).click();
		int afterSize = MyTasksPage.tasksList(driver).size();
		assertEquals("Task should require at least 3 characters, then task should have not been added", initSize,afterSize);
	}
	
	@Test
	public void testMoreThan250Character() {
		int limit = 250;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 251; i++) {
			sb.append('a');
		}
		String input = sb.toString();
		MyTasksPage.newTaskForm(driver).sendKeys(input);
		int size = MyTasksPage.newTaskForm(driver).getAttribute("value").length();
		assertEquals("Task cannot have more than 250 characters", limit, size);
	}
	
	@Test
	public void testAppendTask() {
		String task = "this is a new task";
		MyTasksPage.newTaskForm(driver).sendKeys(task);
		MyTasksPage.newTaskForm(driver).sendKeys(Keys.ENTER);
		String text = MyTasksPage.table(driver).getText();
		String[] lines = text.split("\\n");
		int contain = lines[lines.length-1].indexOf(task);
		assertTrue("Task should have been added to the bottom of the list", contain != -1);		
	}
	
	
	
	@AfterClass
	public static void finish() {
		driver.close();
	}
	
}
