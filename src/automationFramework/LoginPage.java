package automationFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	
	private static WebElement element = null;
	
	public static WebElement emailTextField(WebDriver driver) {
		element = driver.findElement(By.id("user_email"));
		return element;
	}
	
	public static WebElement passwordTextField(WebDriver driver) {
		element = driver.findElement(By.id("user_password"));
		return element;
	}
	
	public static WebElement commitButton(WebDriver driver) {
		element = driver.findElement(By.name("commit"));
		return element;
	}

}
