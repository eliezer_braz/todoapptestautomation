# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

Project to automate two user stories of the application ToDo App.

* Version

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration

In this project, we used the following:
Ubuntu 12.04
Mozilla Firefox 43 and 46 
Selenium 2.53.1
Eclipse  Luna Service Release 2 (4.4.2)

* Dependencies
* Database configuration
* How to run tests

There are three test classes: UserStoryOneTest, UserStoryTwoTest, and RandomTests. The class UserStoryOneTest implements tests for US#1. 	UserStoryTwoTest implements tests for US#2. RandomTests implements other tests for bugs that are not directly related to  US#1 or  US#2.

To run all the tests, use the class TestRunner.

* Deployment instructions

Clone the repository. If you are going to use Eclipse, import the project into your workspace Import --> Existing Project into Workspace and select the folder in which the repository was cloned. 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact